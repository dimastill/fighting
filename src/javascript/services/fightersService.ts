import { callApi } from '../helpers/apiHelper';
import Fighter from '../fighter';
import FightView from '../fightView';

class FighterService {
  async getFighters(): Promise<Array<Fighter>> {
    try {
      const endpoint = 'fighters.json';
      const apiResult = await callApi(endpoint, 'GET');

      return JSON.parse(atob(apiResult.content));
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(_id: number): Promise<Fighter> {
    // implement this method
    let endpoint = `details/fighter/${_id}.json`;
    let apiResult = await callApi(endpoint, 'GET');

    try {
      return JSON.parse(atob(apiResult.content));
    } catch (error) {
      throw error;
    }
  }

  firstInterval: NodeJS.Timeout;
  secondInterval: NodeJS.Timeout;
  isStopFight: boolean = false;

  fight(firstFighter: Fighter, secondFighter: Fighter) {    
    this.firstInterval = setInterval(this.attack, 3000, secondFighter, firstFighter, 'first');
    this.secondInterval = setInterval(this.attack, 3000, firstFighter, secondFighter, 'second');
  }

  attack(attackFighter: Fighter, defenseFighter: Fighter, numberDefenseFighter: string) {
    if (this.isStopFight) {
      clearInterval(this.secondInterval);
      clearInterval(this.firstInterval);
      return;
    }

    let hitPower = attackFighter.getHitPower();
    let blockPower = defenseFighter.getBlockPower()
    if (hitPower - blockPower > 0)
      defenseFighter.health -= (hitPower - blockPower);
    FightView.updateHealthBar(numberDefenseFighter, defenseFighter.health);
    FightView.displayHitPower((hitPower - blockPower), numberDefenseFighter)

    if (defenseFighter.health <= 0) {
      this.isStopFight = true; 
      alert(`${attackFighter.name} WIN!`);
      clearInterval(this.secondInterval);
      clearInterval(this.firstInterval);
      return;
    }
    console.log('Attack: ' + attackFighter.name + `(attack: ${hitPower}). Fighter: ` + defenseFighter.name + `(${defenseFighter.health})`);
  }
}

export const fighterService = new FighterService();
