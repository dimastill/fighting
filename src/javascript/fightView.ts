import View from './view';
import FighterView from './fighterView';
import Fighter from './fighter';

class FightView extends View {
    constructor(firstFighter: Fighter, secondFighter: Fighter) {
        super();

        FightView.firstFighterView = this.createFighter(firstFighter);
        FightView.firstFighterView.style.transform = 'scale(1, 1)';
        FightView.secondFighterView = this.createFighter(secondFighter);
        FightView.secondFighterView.style.transform = 'scale(-1, 1)';
        let firstHealthFighter = this.createHealthBar(firstFighter.health);
        let secondHealthFighter = this.createHealthBar(secondFighter.health);
        FightView.firstFighterView.append(firstHealthFighter);
        FightView.secondFighterView.append(secondHealthFighter);
    }

    static firstFighterView: HTMLElement;
    static secondFighterView: HTMLElement;

    createFighter(fighter: Fighter) {
        const attributes = { src: fighter.source };
        let fighterDiv = this.createElement({tagName: 'div', className: 'fighter-image'})
        const imgElement = this.createElement({
            tagName: 'img',
            className: 'fighter-image',
            attributes
        });

        fighterDiv.append(imgElement);
        return fighterDiv;
    }

    createHealthBar(health: number): HTMLElement {
        let healthFighter = this.createElement({
            tagName: 'div', className: 'healthBar'
        }) ;
        healthFighter.style.width = `${health * 5}px`;
        let currentHealth = this.createElement({
            tagName: 'div', className: 'currentHealthBar'
        });
        currentHealth.style.width = `${health * 5}px`;
        healthFighter.append(currentHealth);
        return healthFighter;
    }

    static updateHealthBar(numberPlayer: string, health: number) {
        let current;
        if (numberPlayer == 'first') {
            current = this.firstFighterView.getElementsByClassName('currentHealthBar')[0];
        }
        else if (numberPlayer == 'second') {
            current = this.secondFighterView.getElementsByClassName('currentHealthBar')[0];
        }
        current.style.width = `${health * 5}px`;
    }

    static displayHitPower(hitPower: number, numberFighter: string) {
        const element: HTMLElement = document.createElement('div');
        element.classList.add('hitPower');
        if (hitPower > 0)
            element.append(`-${hitPower}`);
        else
            element.append('Block');
        if (numberFighter == 'first')
            this.firstFighterView.append(element);
        else if (numberFighter == 'second') {
            this.secondFighterView.append(element);
            element.style.transform = 'scale(-1, 1)'
        }
    }
}

export default FightView;