import View from './view';
import Fighter from './fighter';

class FighterView extends View {
  constructor(fighter: Fighter, handleClick: Function) {
    super();

    this.createFighter(fighter, handleClick);
  }

  createFighter(fighter: Fighter, handleClick: Function) {
    const { name, source } = fighter;
    const nameElement = this.createName(name);
    const imageElement = this.createImage(source);
    const checkboxElement = this.createCheckbox();

    this.element = this.createElement({ tagName: 'div', className: 'fighter' });
    this.element.append(imageElement, nameElement, checkboxElement);
    this.element.addEventListener('click', event => handleClick(event, fighter), false);
  }

  createName(name: string) {
    const nameElement = this.createElement({ tagName: 'span', className: 'name' });
    nameElement.innerText = name;

    return nameElement;
  }

  createImage(source: string) {
    const attributes = { src: source };
    const imgElement = this.createElement({
      tagName: 'img',
      className: 'fighter-image',
      attributes
    });

    return imgElement;
  }

  createCheckbox() {
    return this.createElement({tagName: 'input', className: 'checkboxFighter', attributes: {type: 'checkbox'}})
  }
}

export default FighterView;