class Fighter {
    _id: number;
    name: string;
    health: number;
    attack: number;
    defense: number;
    source: string;
    
    getHitPower(): number {
        return this.attack * (Math.round(Math.random()) + 1);
    }

    getBlockPower(): number {
        return this.defense * (Math.round(Math.random()) + 1);
    }
}

export default Fighter;