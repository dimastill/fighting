import View from './view';
import FighterView from './fighterView';
import { fighterService } from './services/fightersService';
import Fighter from './fighter';

class FightersView extends View {
  constructor(fighters: Array<Fighter>) {
    super();
    
    this.handleFighterClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
  }

  fightersDetailsMap = new Map();

  createFighters(fighters: Array<Fighter>) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleFighterClick);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }
  
  firstFighter: Fighter;
  secondFighter: Fighter;

  async handleFighterClick(event: Event, fighter: Fighter) {
    console.log('clicked');
    this.fightersDetailsMap.set(fighter._id, fighter);
    let selectedFighter = await fighterService.getFighterDetails(fighter._id);
    if (this.firstFighter == undefined) {
      this.firstFighter = new Fighter();
      Object.assign(this.firstFighter, selectedFighter);
      this.setInfoAboutFighter(this.firstFighter, 'first');
    }
    else if (this.secondFighter == undefined) {
      this.secondFighter = new Fighter();
      Object.assign(this.secondFighter, selectedFighter);
      this.setInfoAboutFighter(this.secondFighter, 'second');
    }
    else {
      if (this.firstFighter._id == selectedFighter._id) {
        this.firstFighter = undefined;
        this.setInfoAboutFighter(this.firstFighter, 'first');
      }
      else if(this.secondFighter._id == selectedFighter._id) {
        this.secondFighter = undefined;
        this.setInfoAboutFighter(this.secondFighter, 'second');
      }
    }
  }

  createButtonElement() {
    let button = this.createElement({tagName: 'button', className: 'startButton'});
    button.append('Start');
    return button;
  }

  createFormElement(className: string) {
    let form = this.createElement({tagName: 'form', className: className});
    let labelName = this.createElement({tagName: 'label', className: 'label_name'});
    labelName.append('Name:');
    let textboxName = this.createElement({tagName: 'input', className: 'fighter_name'});
    let labelHealth = this.createElement({tagName: 'label', className: 'label_health'});
    labelHealth.append('Health:');
    let textboxHealth = this.createElement({tagName: 'input', className: 'fighter_health'});
    let labelAttack = this.createElement({tagName: 'label', className: 'label_attack'});
    labelAttack.append('Attack:');
    let textboxAttack = this.createElement({tagName: 'input', className: 'fighter_attack'});
    let labelDefense = this.createElement({tagName: 'label', className: 'label_defense'});
    labelDefense.append('Defense:');
    let textboxDefense = this.createElement({tagName: 'input', className: 'fighter_defense'});
    form.append(labelName, textboxName, labelHealth, textboxHealth,
       labelAttack, textboxAttack, labelDefense, textboxDefense);
      
    return form;
  }

  setInfoAboutFighter(fighter: Fighter, numberFighter: string) {
    let form: HTMLCollectionOf<Element> = document.getElementsByClassName(`${numberFighter}_fighter_info`);
    
    let inputName: HTMLInputElement = 
      (<HTMLInputElement>form[0].getElementsByClassName('fighter_name')[0]);
    let inputHealth: HTMLInputElement = 
      (<HTMLInputElement>form[0].getElementsByClassName('fighter_health')[0]);
    let inputAttack: HTMLInputElement = 
      (<HTMLInputElement> form[0].getElementsByClassName('fighter_attack')[0]);
    let inputDefense: HTMLInputElement = 
      (<HTMLInputElement> form[0].getElementsByClassName('fighter_defense')[0]);
    
    if (fighter != undefined) {
      inputName.value = fighter.name;
      inputHealth.value = String(fighter.health);
      inputAttack.value = String(fighter.attack);
      inputDefense.value = String(fighter.defense);
    }
    else {
      inputName.value = '';
      inputHealth.value = '';
      inputAttack.value = '';
      inputDefense.value = '';
    }
  }
}

export default FightersView;