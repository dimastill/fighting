import FightersView from './fightersView';
import { fighterService } from './services/fightersService';
import FightView from './fightView';

class App {
  constructor() {
    this.startApp();
  }

  static rootElement = document.getElementById('root');
  static loadingElement = document.getElementById('loading-overlay');
  static fightViewElement = document.getElementById('fightView');

  async startApp() {
    try {
      App.loadingElement.style.visibility = 'visible';
      
      const fighters = await fighterService.getFighters();
      const fightersView = new FightersView(fighters);
      const fightersElement = fightersView.element;

      App.rootElement.appendChild(fightersElement);

      let firstFighterInfo: Node = fightersView.createFormElement('first_fighter_info');
      App.rootElement.append(firstFighterInfo);

      let startButton: Node = fightersView.createButtonElement();
      App.rootElement.appendChild(startButton);
      startButton.addEventListener('click', event => {
        App.rootElement.style.visibility = 'hidden';
        App.fightViewElement.style.visibility = 'visible';
        const fightView: FightView = 
          new FightView(fightersView.firstFighter, fightersView.secondFighter);

        App.fightViewElement.append(FightView.firstFighterView, FightView.secondFighterView);

        fighterService.fight(fightersView.firstFighter, fightersView.secondFighter);
      });

      let secondFighterInfo: Node = fightersView.createFormElement('second_fighter_info');
      App.rootElement.append(secondFighterInfo);
    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = 'Failed to load data';
    } finally {
      App.loadingElement.style.visibility = 'hidden';
    }
  }
}

export default App;