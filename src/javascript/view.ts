class View {
  element: HTMLElement;

  createElement({ tagName, className = '', attributes = {} }): HTMLElement {
    const element: HTMLElement = document.createElement(tagName);
    element.classList.add(className);
    Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));

    return element;
  }
}

export default View;
